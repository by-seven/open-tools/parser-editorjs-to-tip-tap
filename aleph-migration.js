// FiXME: there is a bug of synchronization
// when I run the program for the first time it skips 16 EditorContent
// when I run it the second time it skips 1 EditorContent
import pg from 'pg'
import sqlite3 from 'sqlite3'
import editorJsParserToTiptap from './parser.js'

const sqliteVerbose = sqlite3.verbose()

const BATCH_ROWS = 100

const alephTableCache = 'aleph_editor_content_parsed'
const remoteTableEditor = 'editor_contents'

async function alreadyInjectedEditorModule(localDb, editorModuleId) {
  return new Promise((resolve, reject) => {
    localDb.all(`SELECT * FROM ${alephTableCache} WHERE editor_module_id = ?`, editorModuleId, (err, rows) => {
      if (rows?.length > 0)
        resolve(true)
      else
        resolve(false)
    })
  })
}

async function saveEditorModuleInjected(localDb, editorModuleId) {
  return new Promise((resolve, reject) => {
    localDb.all(`INSERT INTO ${alephTableCache} (editor_module_id) VALUES (?)`, editorModuleId, () => {
      resolve()
    })
  })
}

async function makeRequest(client, localDb, offset = 0) {
  const sql = `SELECT DISTINCT * FROM ${remoteTableEditor} WHERE blocks IS NOT NULL LIMIT ${BATCH_ROWS} OFFSET ${offset}`
  const res = await client.query(sql)

  console.log(`executed: ${sql}`)
  if (res.rows.length === 0) return

  for (const editorModuleKey in res.rows) {
    const editorModule = res.rows[editorModuleKey]

    const alreadyInjected = await alreadyInjectedEditorModule(localDb, editorModule?.id)
    if (alreadyInjected)
      continue

    const html = editorJsParserToTiptap(editorModule?.blocks)

    try {
      const resUpdate = await client.query(`UPDATE ${remoteTableEditor} set blocks = to_json($1::text)::jsonb WHERE id = $2`, [html, editorModule.id])
      await saveEditorModuleInjected(localDb, editorModule.id)
      // Here save in sql
    } catch (e) {
      console.error(`There was an error while updating editor module id: ${editorModule.id}`)
      console.error(e)
    }
  }

  await makeRequest(client, localDb, offset + BATCH_ROWS)
}

async function setupDb(hostname) {
  const db = new sqliteVerbose.Database(hostname)

  return new Promise((resolve, reject) => {
    db.serialize(() => {
      db.run(`CREATE TABLE IF NOT EXISTS ${alephTableCache} (editor_module_id int)`, () => {
        resolve(db)
      })
    })
  })
}

async function main() {
  const databaseUrl = process.env.DATABASE_URL;

  if (!databaseUrl) {
    throw 'You must define DATABASE_URL'
  }

  const url = new URL(databaseUrl)
  const client = new pg.Client({
    ssl: url.host === 'localhost:5432' ? false :  { rejectUnauthorized: false },
    connectionString: databaseUrl,
  })
  await client.connect()
  const localDb = await setupDb(url.hostname)

  try {
    await makeRequest(client, localDb)
  } catch (err) {
    console.error(err);
  } finally {
    await client.end()
    await localDb.close()
  }
}
main()