import pg from 'pg'
import sqlite3 from 'sqlite3'
import editorJsParserToTiptap from './parser.js'

const sqliteVerbose = sqlite3.verbose()

const BATCH_ROWS = 100

const studioTableCache = 'studio_editor_modules_parsed'

async function alreadyInjectedEditorModule(localDb, editorModuleId) {
  return new Promise((resolve, reject) => {
    localDb.all(`SELECT * FROM ${studioTableCache} WHERE editor_module_id = ?`, editorModuleId, (err, rows) => {
      if (rows?.length > 0)
        resolve(true)
      else
        resolve(false)
    })
  })
}

async function saveEditorModuleInjected(localDb, editorModuleId) {
  return new Promise((resolve, reject) => {
    localDb.all(`INSERT INTO ${studioTableCache} (editor_module_id) VALUES (?)`, editorModuleId, () => {
      resolve()
    })
  })
}

async function makeRequest(client, localDb, offset = 0) {
  const sql = `SELECT DISTINCT * FROM editor_modules WHERE data IS NOT NULL OR ((instructions IS NOT NULL AND trim(instructions) != '') AND data IS NULL) LIMIT ${BATCH_ROWS} OFFSET ${offset}`
  const res = await client.query(sql)

  console.log(`executed: ${sql}`)
  if (res.rows.length === 0) return

  for (const editorModuleKey in res.rows) {
    const editorModule = res.rows[editorModuleKey]

    const alreadyInjected = await alreadyInjectedEditorModule(localDb, editorModule?.id)
    if (alreadyInjected)
      continue

    const html = (editorModule?.instructions && editorModule.instructions !== '') ? editorJsParserToTiptap(editorModule.instructions) : editorJsParserToTiptap(editorModule?.data?.blocks)

    try {
      const resUpdate = await client.query("UPDATE editor_modules set data = to_json($1::text)::jsonb WHERE id = $2", [html, editorModule.id])
      await saveEditorModuleInjected(localDb, editorModule.id)
      // Here save in sql
    } catch (e) {
      console.error(`There was an error while updating editor module id: ${editorModule.id}`)
      console.error(e)
    }
  }

  await makeRequest(client, localDb, offset + BATCH_ROWS)
}

async function setupDb(hostname) {
  const db = new sqliteVerbose.Database(hostname)

  return new Promise((resolve, reject) => {
    db.serialize(() => {
      db.run(`CREATE TABLE IF NOT EXISTS ${studioTableCache} (editor_module_id int)`, () => {
        resolve(db)
      })
    })
  })
}

async function main() {
  const databaseUrl = process.env.DATABASE_URL;

  if (!databaseUrl) {
    throw 'You must define DATABASE_URL'
  }

  const url = new URL(databaseUrl)
  const client = new pg.Client({
    ssl: url.host === 'localhost:5432' ? false :  { rejectUnauthorized: false },
    connectionString: databaseUrl,
  })
  await client.connect()
  const localDb = await setupDb(url.hostname)

  // localDb.all('SELECT DISTINCT * FROM editor_modules_parsed', (err, rows) => {
  //   console.log(rows?.length)
  //   throw 'test'
  // })


  try {
    await makeRequest(client, localDb)
  } catch (err) {
    console.error(err);
  } finally {
    await client.end()
    await localDb.close()
  }
}
main()