import editorJsParser from "./vendors/editorjs-html/app.js";

function replaceStrToSpan(str) {
  let toRet = ''

  if (typeof str !== 'string')
    return str

  toRet = str.replace(/<font/g, '<span')
  toRet = toRet.replace(/<\/font>/g, '</span>')
  toRet = toRet.replace(/<p>/g, '<div>')
  toRet = toRet.replace(/<\/p>/g, '</div>')
  toRet = toRet.replace(/span-weight/g, 'font-weight')
  toRet = toRet.replace(/text-decoration-line/g, 'text-decoration')

  return toRet
}

function transformInlineFontTag(element) {
  if (!element?.data?.text) return element

  element.data.text = replaceStrToSpan(element?.data?.text)
  return element
}

export default (blocks = []) => {
  if (typeof blocks === 'string') {
    return replaceStrToSpan(blocks)
  }
  if (!Array.isArray(blocks))
    return blocks

  let finalHTML = '';

  const inlineImageParser = (block) => {
    return `<img src="${block?.data?.url}" />`
  }

  const dividerParser = (block) => {
    return `<hr class="${block?.data?.fullWidth ? 'w-full' : 'w-1/2'} my-4 flex items-center justify-center h-0.5 bg-middleGrey transition-all duration-200" />`
  }

  const rawParser = (block) => {
    return `<pre><code>${block?.data?.html}</code></pre>`
  }

  const checklistParser = (block) => {
    const listStyle = "ul";

    let list = ""
    for (let item of block.data.items) {
      let element = `<li data-type="taskItem" data-checked="${item.checked}">${item.text}</li>`
      list += element
    }

    return `<${listStyle} data-type="taskList">${list}</${listStyle}>`;
  }

  const quoteParser = (block) => {
    return `<blockquote>${block?.data?.text}</blockquote>`
  }

  const listParser = (block) => {
    const listStyle = block.data.style === "unordered" ? "ul" : "ol";

    const recursor = (items, listStyle) => {
      const list = items.map(item => {
        if (!item?.content && !item?.items?.length) return `<li>${item}</li>`;

        let list = "";
        if (item?.items?.length) list = recursor(item?.items, listStyle);
        if (item?.content) return `<li>${item?.content}</li>` + list;
      });

      return `<${listStyle}>${list.join("")}</${listStyle}>`;
    };

    return recursor(block.data.items, listStyle);
  }

  const tableParser = (block) => {
    let tableHtml = ''
    const tableStart = '<table><tbody>'
    const tableEnd = '</tbody></table>'

    tableHtml += tableStart
    for (const contentKey in block.data.content) {
      const rowStart = '<tr>'
      const rowEnd = '</tr>'
      const cellStart = parseInt(contentKey) === 0 ? '<th>' : '<td>'
      const cellEnd = parseInt(contentKey) === 0 ? '</th>' : '</td>'

      tableHtml += rowStart
      for (const contentElementKey in block.data.content[contentKey]) {
        tableHtml += `${cellStart}${block.data.content[contentKey][contentElementKey]}${cellEnd}`
      }
      tableHtml += rowEnd
    }
    tableHtml += tableEnd

    return tableHtml
  }

  for (let element of blocks) {
    element = transformInlineFontTag(element)


    if (element.type === "inlineImage") {
      let result = editorJsParser({ inlineImage: inlineImageParser }).parseBlock(element);
      finalHTML += result;
    }  else if (element.type === "divider") {
      let result = editorJsParser({ divider: dividerParser }).parseBlock(element);
      finalHTML += result;
    } else if (element.type === "raw") {
      let result = editorJsParser({ raw: rawParser }).parseBlock(element);
      finalHTML += result;
    } else if (element.type === "list") {
      let result = editorJsParser({ list: listParser }).parseBlock(element);
      finalHTML += result;
    } else if (element.type === "checklist") {
      let result = editorJsParser({ checklist: checklistParser }).parseBlock(element);
      finalHTML += result;
    } else if (element.type === "quote") {
      let result = editorJsParser({ quote: quoteParser }).parseBlock(element);
      finalHTML += result;
    } else if (element.type === "table") {
      let result = editorJsParser({ table: tableParser }).parseBlock(element);
      finalHTML += result;
    } else {
      let result = editorJsParser().parseBlock(element);
      finalHTML += result;
    }
  }
  return finalHTML;
}

